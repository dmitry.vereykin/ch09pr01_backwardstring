/**
 * Created by Dmitry on 7/15/2015.
 */
import java.util.Scanner;

public class BackwardString {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        String input;
        System.out.print("Enter a string: ");
        input = keyboard.nextLine();
        System.out.print("\nString backwards: ");
        reverse(input);
        keyboard.close();
    }

    private static void reverse(String str){
        char[] array = str.toCharArray();
        for (int n = array.length - 1; n >= 0; n--) {
            System.out.print(array[n]);
        }
    }
}
